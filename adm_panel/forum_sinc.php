<?php

if (!defined('H'))
    define('H', $_SERVER['DOCUMENT_ROOT'] . '/');
include_once H . 'sys/inc/start.php';
include_once H . 'sys/inc/compress.php';
include_once H . 'sys/inc/sess.php';
include_once H . 'sys/inc/settings.php';
include_once H . 'sys/inc/db_connect.php';
include_once H . 'sys/inc/ipua.php';
include_once H . 'sys/inc/fnc.php';
include_once H . 'sys/inc/adm_check.php';
include_once H . 'sys/inc/user.php';
user_access('adm_forum_sinc', null, 'index.php?' . SID);
adm_check();
$set['title'] = 'Синхронизация таблиц форума';
include_once H . 'sys/inc/thead.php';
title();
err();
aut();
if (isset($_GET['ok']) && isset($_POST['accept']))
{
    $d_r = 0;
    $d_t = 0;
    $d_p = 0;
    $d_j = 0;
    // удаление разделов
    if (mysql_query("DELETE FROM `forum_razdels` WHERE `id_forum` NOT IN(SELECT `id` FROM `forum`)"))
    {
        $d_r = mysql_affected_rows();
    }
    // удаление тем
    if (mysql_query("DELETE FROM `forum_themes` WHERE `id_razdel` NOT IN(SELECT `id` FROM `forum_razdels`)"))
    {
        $d_t = mysql_affected_rows();
    }
    // удаление постов
    if (mysql_query("DELETE FROM `forum_posts` WHERE `id_theme` NOT IN(SELECT `id` FROM `forum_themes`)"))
    {
        $d_p = mysql_affected_rows();
    }
    // удаление несуществующих тем из журнала
    if (mysql_query("
    DELETE FROM `forum_journal` WHERE `id_theme` NOT IN(
        SELECT `id` FROM `forum_themes`) 
    OR `id_user` NOT IN(
        SELECT `id` FROM `user`)
        "))
    {
        $d_j = mysql_affected_rows();
    }
    msg('Удалено разделов: ' . $d_r . ', тем: ' . $d_t . ', постов: ' . $d_p);
}
?>
<div class="p_m">
<form method="post" action="?ok">
    <fieldset>
        <legend>Очистка таблиц форума</legend>
        <input value="Начать" name="accept" type="submit" />
    </fieldset>
</form>
    <strong>* В зависимости от количества сообщений и тем, данное действие может занять длительное время.</strong><br />
    <strong>** Рекомендуется использовать только в случах расхождений счетчиков форума с реальными данными</strong><br />
</div>
<?
if (user_access('adm_panel_show'))
{
    echo "<div class='foot'>\n";
    echo "&laquo;<a href='/adm_panel/'>В админку</a><br />\n";
    echo "</div>\n";
}
include_once H . 'sys/inc/tfoot.php';

?>