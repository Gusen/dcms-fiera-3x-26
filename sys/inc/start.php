<?php

$mysqli = false;
$install_mod = false;
define('MEMORY_START', memory_get_usage());
define('SYSTEM', 'FIERA');

date_default_timezone_set('Europe/Moscow');
if (function_exists('error_reporting')) error_reporting(0); // отключаем показ ошибок
// Ставим ограничение для выполнения скрипта на 60 сек
if (function_exists('set_time_limit')) set_time_limit(60);
if (function_exists('ini_set'))
{
    ini_set('display_errors', true); // отключаем показ ошибок
    ini_set('register_globals', false); // вырубаем глобальные переменные
    ini_set('session.use_cookies', true); // используем куки для сессий
    ini_set('session.use_trans_sid', true); // используем url для передачи сессий
    ini_set('arg_separator.output', "&amp;"); // разделитель переменных в url (для соответствия с xml)
}
list($msec, $sec) = explode(chr(32), microtime()); // время запуска скрипта
$conf['headtime'] = $sec + $msec;
$time = time();
$phpvervion = explode('.', phpversion());
$conf['phpversion'] = $phpvervion[0];
$upload_max_filesize = ini_get('upload_max_filesize');
if (preg_match('#([0-9]*)([a-z]*)#i', $upload_max_filesize, $varrs))
{
    if ($varrs[2] == 'M') $upload_max_filesize = $varrs[1] * 1048576;
    elseif ($varrs[2] == 'K') $upload_max_filesize = $varrs[1] * 1024;
    elseif ($varrs[2] == 'G') $upload_max_filesize = $varrs[1] * 1024 * 1048576;
}
function fiera($msg)
{
    $msg = str_replace("script", "sсript", $msg);
    $msg = str_replace("javаscript:", "javаscript:", $msg);
    if ($_SERVER['PHP_SELF'] != '/adm_panel/mysql.php') $msg = addslashes(stripslashes
            (trim($msg)));
    return $msg;
}
